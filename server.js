#!/usr/bin/env node
var WebSocketServer = require('ws').Server
  , http = require('http')
  , express = require('express')
  , app = express()
  , port = process.env.PORT || 5000
  , pg = require('pg');

app.use(express.static(__dirname + '/'));

var server = http.createServer(app);
server.listen(port);

console.log('http server listening on %d', port);
var currentState = null;

var wss = new WebSocketServer({server: server});
wss.broadcast = function(data, me) {
  for(var i in this.clients) {
    // don't broadcast to the client generating the request.
    if(this.clients[i] != me) {
      this.clients[i].send(data);
    }
  }
};

console.log('websocket server created');
wss.on('connection', function(ws) {
    console.log('websocket connection open');
    if(currentState) {
      ws.send(currentState);
    }

    ws.on('message', function(message) {
      currentState = message;
      // receive sensor reading.
      console.log('received: %s', message);
      // send it to any connected clients.
      if(currentState) {
        wss.broadcast(currentState, ws);
      }
    });

    ws.on('close', function() {
      console.log('websocket connection close');
    });
});

/*
function saveState(location, state) {
  pg.connect(process.env.DATABASE_URL, function(err, client, done) {
    client.query('INSERT INTO sensor_log(date, location, value) VALUES ($1, $2, $3)', [new Date(), location, state], function(err, result) {
      done();
      if(err) {
        return console.error(err);
      }
    });
  });
}
*/
