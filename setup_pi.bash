#
# The purpose of this script is to get the raspberry pi ready to be a honeypot sensor.
#


# Start with a fresh raspbian image
# Enable ssh
# change password

# do updates.
sudo apt-get --assume-yes update
sudo apt-get --assume-yes upgrade

# install node and git
sudo apt-get --assume-yes install nodejs
sudo apt-get --assume-yes install npm
sudo apt-get --assume-yes install git

# optional
sudo apt-get --assume-yes install vim

# install node gpio.
cd ~
git clone git://github.com/quick2wire/quick2wire-gpio-admin.git
cd quick2wire-gpio-admin
make
sudo make install
sudo adduser $USER gpio

# get honeypot code.
cd ~
git clone https://bitbucket.org/josephdwyer/honeypot.git
cd honeypot
npm install ws
npm install rpi-gpio

# url of the service. (my machine name for testing).
#echo 'export WS_URL="ws://goblin-cleaver.herokuapp.com"' >> ~/.profile
