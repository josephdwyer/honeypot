#Honeypot  
The goal of this project is to solve an important problem, how do you know if the ONLY men's stall all the way across the building is in use?  

##Requirements    
###Server  
* heroku account  
###Sensor  
* Raspberry Pi running raspian  
* Reed switch e.g. https://www.sparkfun.com/products/8642  
* Female to Female jumper wires e.g. https://www.sparkfun.com/products/8430  
* Neodymium magnet  


##Instructions  
###Server  
1. Clone this repository.  
1. Create heroku application.  
1. Add your heroku application remote to this git repository.  
1. Push to heroku remote.  

###Sensor  
1. Connect a female jumper pin from +3.3V pin to reed switch and one from reed switch to the GPIO pin you want to use.  
1. Run setup scipt in raspberry pi terminal ```bash <(curl -s https://bitbucket.org/josephdwyer/honeypot/raw/master/setup_pi.bash)```  
1. Set configuration environment variables to your settings  
    ```  
    echo 'export WS_URL="ws://goblin-cleaver.herokuapp.com"' >> ~/.profile  
    echo 'export SENSOR_LOCATION="ebi"' >> ~/.profile  
    echo 'export PIN_NBR="7"' >> ~/.profile  
    ```  
