#!/usr/bin/env node
var WebSocket = require('ws');
var gpio = require("rpi-gpio");


function monitor() {
  try {
    var gpioReady = false;
    var websocketReady = false;
    var previousValue = null;

    gpio.setup(process.env.PIN_NBR, gpio.DIR_IN, function() {
      gpioReady = true;
    });

    var ws = new WebSocket(process.env.WS_URL);

    ws.on('open', function() {
      console.log('connection opened');
      websocketReady = true;
    });

    ws.on('message', function(data, flags) {
      // flags.binary will be set if a binary data is received
      // flags.masked will be set if the data was masked
      console.log('message recieved: ' + data);
    });

    ws.on('close', function() {
      gpioReady = false;
      websocketReady = false;
      console.log('connection closed');
      checkInterval = checkInterval ? clearInterval(checkInterval) : null;
      setTimeout(monitor, 30000);
      gpio.destroy();
    });

    var checkInterval = setInterval(function() {
      if(!gpioReady) {
        console.log('gpio not ready');
        return;
      }
      if(!websocketReady) {
        console.log('websocket not ready');
        return;
      }
      checkSensor(function(value) {
        if(value != previousValue) {
          ws.send(JSON.stringify(value));
          console.log("sending: " + value);
          previousValue = value;
        }
      });
    }, 1000);
  }
  catch(e) {
    gpioReady = false;
    websocketReady = false;
    console.log("error: " + e);
    checkInterval = checkInterval ? clearInterval(checkInterval) : null;
    setTimeout(monitor, 30000);
    gpio.destroy();
  }
}

function checkSensor(callback) {
  gpio.read(process.env.PIN_NBR, function(err, value) {
    if(err) {
      console.log("error" + err);
      callback(false);
    }
    else {
      callback(value);
    }
  });
}

monitor();
